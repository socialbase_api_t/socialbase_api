#!/usr/bin/env python3
import os
import sys
from string import Template

# gist from https://gist.github.com/charmoniumQ/fb489cdfb13836918040144cb63e266b
out_line = ""
for in_line in sys.stdin.readlines():
    out_line += Template(in_line).safe_substitute(os.environ)
sys.stdout.write(out_line)
