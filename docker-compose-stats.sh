# gist from https://gist.github.com/petermodzelewski/1567a711a9f26a5764a7
docker-compose ps | grep Up | awk '{print $1}' | tr "\\n" " " | xargs --no-run-if-empty docker stats