#!/bin/bash

echo "Install requirements file"
pip install -r $REQUIREMENTS

# Collect static files
# echo "Collect static files"
# python manage.py collectstatic --noinput
# python manage.py collectmedia --noinput


# Apply database migrations
echo "Apply database migrations"
python manage.py makemigrations

#------------------
# wait-for-postgres
until python manage.py migrate; do
  >&2 echo "Postgres may be unavailable - sleeping"
  sleep 5
done

>&2 echo "Postgres is up - executing command"
#------------------

# Start server
echo "Starting server"
python manage.py runserver 0.0.0.0:$PORT

# export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE

# exec gunicorn $PROJECT_WSGI_MODULE:application -w 2 -b 0.0.0.0:$PORT --name "$COMPOSE_PROJECT_NAME"