#-*- coding: utf-8 -*-

# Arquivo fabfile.py
from datetime import datetime
# aqui importando uns caras legais da api do fabric
# local - roda um comando de shell na máquina local
# run - roda um comando de shell no servidor remoto
# put - faz uma cópia via ssh (scp) pro servidor remoto
# env - configurações do ambiente
from fabric.api import local
from os.path import exists


def makemigrations(app):
    local("./manage.py makemigrations {app}".format(app=app))

def migrations():
    local("./manage.py makemigrations")
    local("./manage.py migrate")


def run(port=8000):
    local("./manage.py runserver {port}".format(port=port))


def fix(name):
    local("./manage.py fix_{name}".format(name=name))


def config_db():
    migrations()
    local("./manage.py fix_ocorrencia")
    run()


def drop_db():
    local("./manage.py flush --noinput")


def collect():
    local("./manage.py collectstatic --noinput")
    local("./manage.py collectmedia --noinput")

def test():
    local("./manage.py test -- -s")

def dev():
    drop_db()
    migrations()
    # collect()
    fix("tweet")

    run()


def prod():
    migrations()


def backup_db():
    today_label = datetime.now().strftime("%Y-%m-%d_%H-%M")
    local("mkdir -p _backups_db")
    local("./manage.py dumpdata --exclude contenttypes --indent=1 > _backups_db/backup_db_{}.json".format(today_label))


def rm_sqlite():
    local("rm project/db.sqlite3")

def shell():
    local("./manage.py shell_plus")

def test(modulo=None, clazz=None):
    if modulo and not clazz:
        local("./manage.py test {modulo}".format(modulo=modulo))
    elif modulo and clazz:
        local("./manage.py test {modulo}:{clazz}".format(modulo=modulo, clazz=clazz))
    else:
        local("./manage.py test")

