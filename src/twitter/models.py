# -*- coding: utf-8 -*-
from django.db import models

class Tweet(models.Model):
    class Meta:
        verbose_name = u'Tweet'
        verbose_name_plural = u'Tweets'

    user = models.ForeignKey('auth.User')
    text = models.CharField(max_length=140)

    def __str__(self):
        return self.text    