# -*- coding: utf-8 -*-
from django.test import TestCase
from model_mommy import mommy
from django.utils.timezone import datetime
from twitter.models import Tweet
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from rest_framework import status
import datetime
from django.utils import timezone

from oauth2_provider.models import (
    get_access_token_model, get_application_model,
    get_grant_model, get_refresh_token_model, AccessToken
)

class TestTweet(APITestCase):

    def __create_authorization_header(token):
        return "Bearer {0}".format(token)

    def setUp(self):
        self.superuser = User.objects.create_superuser('admin ', 'admin@email.com', '123123')
        Application = get_application_model()                  
        self.app = Application(
            name="test_app",
            redirect_uris="http://localhost http://example.com http://example.org",
            user=self.superuser,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_id="YsUns9jisQeb8DIstVIs58XFp4JRADQXG5WrqsVC",
            client_secret="6xJv7mSNEIPQgSeBsak6isx1oNiwOanQYkVeJSxBoHYoNCfosuupl3ik0oxwWoi5NZ8z9pUDyo9KTAaHyqlSYC8t5zRgvGqjMuSDUFn9EmYoFZm4SQAPdtm0vg1RIj0X",
        )  
        self.app.save() 
        self.access_token = AccessToken.objects.create(
            user=self.superuser,
            scope='read write',
            expires=timezone.now() + datetime.timedelta(seconds=30000),
            token='1234567890',
            application=self.app
        )        

    def _create_authorization_header(self, token):
        return "Bearer {0}".format(token)

    def test_create_tweet_post(self):
        auth = self._create_authorization_header(self.access_token.token)
        data = {'user':self.superuser.id, 'text':'remember the milk'}
        response = self.client.post('/api/twitter/', data, format='json', HTTP_AUTHORIZATION=auth)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)  

    def test_create_tweet_count(self):  
        self.test_create_tweet_post() 
        response = self.client.get('/api/twitter/')
        self.assertEqual(len(response.data), 1)                

    def test_create_tweet_qualify(self):

        self.test_create_tweet_post() 
        response = self.client.get('/api/twitter/')
        expected_data = {'id':1, 'text':'remember the milk','user':self.superuser.id}
        entry = response.data[0]
        self.assertEqual(entry, expected_data)       
    
      


