# -*- coding: utf-8 -*-
from django.test import TestCase
from model_mommy import mommy
from django.utils.timezone import datetime
from twitter.models import Tweet

class TestTweet(TestCase):
  
  def setUp(self):
      self.tweet = mommy.make(Tweet, text='Teste 1')
      
  def test_record_creation(self):
      self.assertTrue(isinstance(self.tweet, Tweet))
      self.assertEquals(self.tweet.__str__(), self.tweet.text)