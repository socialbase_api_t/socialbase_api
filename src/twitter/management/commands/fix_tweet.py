# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User, Permission

from twitter.models import Tweet
import datetime as date

from django.db import models

from oauth2_provider.models import (
    get_access_token_model, get_application_model,
    get_grant_model, get_refresh_token_model
)

class Command(BaseCommand):
    help = 'Cenário de desenvolvimento de Tweets'

    def handle(self, **options):
        self.stdout.write('Criando Tweets')

        superuser = User.objects.create_superuser(
            'admin', 'admin@email.com.br', '123123'
        )

        tweet_1, created = Tweet.objects.get_or_create(user=superuser,
            text = u"Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum")

        tweet_2, created = Tweet.objects.get_or_create(user=superuser,
            text = u"Lorem_2 ipsum Lorem ipsum Lorem ipsum Lorem ipsum")  

        # Auxiliar Testes em Dev
        Application = get_application_model()                  
        app = Application.objects.create(
            name="test_app",
            redirect_uris="http://localhost http://example.com http://example.org",
            user=superuser,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_id="YsUns9jisQeb8DIstVIs58XFp4JRADQXG5WrqsVC",
            client_secret="6xJv7mSNEIPQgSeBsak6isx1oNiwOanQYkVeJSxBoHYoNCfosuupl3ik0oxwWoi5NZ8z9pUDyo9KTAaHyqlSYC8t5zRgvGqjMuSDUFn9EmYoFZm4SQAPdtm0vg1RIj0X",
        )  
        app.save()     