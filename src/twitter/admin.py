# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Tweet


class TweetAdmin(admin.ModelAdmin):
    list_display = ('user', 'text',)
    search_fields = ('user', )    


admin.site.register(Tweet, TweetAdmin) 