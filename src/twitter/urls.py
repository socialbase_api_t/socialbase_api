#-*- coding: utf-8 -*-
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers

from . import views
from twitter import views

router = routers.DefaultRouter()
router.register(r'twitter', views.TweetViewSet)

urlpatterns = [
	url(r'^api/', include(router.urls)),
]