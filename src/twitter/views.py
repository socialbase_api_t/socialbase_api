# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from .models import Tweet
from rest_framework import viewsets
from .serializers import TweetSerializer
from .permissions import IsAuthenticatedOrCreate


class TweetViewSet(viewsets.ModelViewSet):
    """
    API endpoint.
    """
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer  
    permission_classes = (IsAuthenticatedOrCreate,)  
