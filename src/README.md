# SocialBase Api

Principais Tecnologias Utilizadas:
==================================

    - Python 3
    - Django==1.11.7
    - Django RestFramework
    - Fabric
    - Pillow
    - SQLITE
    - django-rest-framework-social-oauth2
    - HTML/CSS/Javascript 

obs: Poderá avaliar o projeto com ou sem Docker, começamos sem Docker:

Sem Docker:
===========

1) Constraints:

    - Precisará ter Python 3 instalado na sua máquina.
    - Precisará instalar o Virtualenv (Um pacote para criar ambientes virtuais para Python)  

2) Como rodar o software em sua máquina local (obs: Não foi testado em Máquina Windows):

    - virtualenv vm (criando a vm)
    
    - pip install -r project/requirements/dev.txt (este passo, instala a bibliotecas no projeto)
    
    - fab dev (este passo, utiliza o fabric [automatizador de processo] que chama a função dev no script fabfile.py). A função dev, deleta o banco de dados atual, roda migrations e migrate [função para criação e evolução da base de dados], executa fix_tweet [isso cria alguns tweets de teste, um super usuário de teste e uma aplicação para auth2], chama o micro servidor default do Django em localhost:8000). 

Com Docker:
===========

1) Docker (v17.04.0+)

Siga as instruções oficiais para instalar a [Docker Engine](https://docs.docker.com/engine/installation/), de acordo com seu Sistema Operacional (atenção para versões de distros diferentes de Linux, que podem ter passos diferentes entre si).

2) Docker Compose (v1.15.0+)

Existem várias formas diferentes de instalar o Compose (veja na documentação oficial).
A mais prática é instalar via "pip". Recomenda-se a utilização de uma virtualenv para isso.

```
pip install docker-compose
```

Certifique-se que você possui tanto a [Docker Engine](https://docs.docker.com/engine/installation/) (v17.04.0+) quanto o [Docker Compose](https://docs.docker.com/compose/install/) (v1.15.0+) instalados em sua máquina.

```
$ docker -v 
$ docker-compose -v
```

Caso um dos dois não esteja instalado, instale-os seguindo as instruções dos links acima.

Instalação
==========

Baixe o projeto: 

```
$ git clone https://gitlab.com/xcx <NOME_DO_SEU_PROJETO>
```

## Rodando o Projeto 

Agora é preciso definir algumas váriais de ambiente antes de rodar o projeto. A forma mais indicada de se fazer isso é criar na raiz do projeto, no mesmo nível do arquivo  **docker-compose.yml**, um arquivo com o nome de  **.env** e definir essas váriaves.

Para facilitar, existe um arquivo na raiz deste repositório chamado **.env.example**, que já indica as variáveis que podem (**e devem**) ser alteradas de acordo com o projeto, como nome (usado sobretudo para diferenciar múltiplos containers na mesma máquina), porta, informações do database, configurações do Django, etc., com estrutura semelhante a isso: 


```
## Project

COMPOSE_PROJECT_NAME=<nome_do_projeto> 
PORT=<porta_do_projeto>

## DataBase
POSTGRES_PASSWORD=<senha_para_db>
POSTGRES_USER=<user_para_db>
POSTGRES_DB=<nome_para_db>

## Django Settings (develop)
DEBUG=True
REQUIREMENTS=requirements/dev.txt
DJANGO_SETTINGS_MODULE=project.settings.dev
PROJECT_WSGI_MODULE=project.wsgi.dev
ALLOWED_HOSTS=localhost
```

Feito isso, ainda na pasta raiz, execute o seguinte comando para criar as imagens docker necessárias ao projeto:

```
$ docker-compose build
$ docker-compose up
```

Se o comando acima foi executado com sucesso, agora basta executar:

```
$ docker-compose up
```

**Obs:** Caso essa seja a primeira vez que você está fazendo esse passo-a-passo, esse processo pode levar vários minutos pois o docker precisa baixar as imagens externas *(redis, postgres, nginx, python:2.7)* e configurar a imagem local *(django_core)*.


Considerando que no arquivo **.env** foi colocada a porta 8000, agora basta acessar *localhost:8000* para visualizar a aplicação.

## Povoando o banco (executando scripts no container)

### Executando comandos de shell nos containers

A forma mais **fácil e preferencial** para executar comandos no shell do projeto é utilizar o script **docker-shell.sh**, passando como parâmetro o service do docker compose (descrito no arquivo *docker-compose.yml*). Por padrão, caso não passe parâmetros, será aberto o shell para o app django):

```
$ ./docker-shell.sh
```

Caso tenha entrado no shell com sucesso, basta chamar um comando de script que já executa os migrations e algumas fixtures django para criar os usuários básicos do sistema, e um cenário inicial para testes.

```
$ fab dev
```

## Entendendo os containers

Você agora tem em mãos quatro containers docker rodando em composição, um redis, um postgres, um python e um nginx com toda a base do core configurada e instalada (requirements, collectstatic inicial e migrations inicial). Seus containers terão as seguintes características:

<table>
 <tr>
    <th>Serviço</th>
    <th>Imagem</th> 
    <th>Nome</th>
  </tr>
 <tr>
    <td>cache</td>
    <td>redis</td> 
    <td>nome_do_projeto_cache</td>
  </tr>
  <tr>
    <td>db</td>
    <td>postgres</td> 
    <td>nome_do_projeto_db</td>
  </tr>
  <tr>
    <td>app</td>
    <td>django_core<br/>(Feita a partir do python:2.7)</td> 
    <td>nome_do_projeto_app</td>
  </tr>
  <tr>
    <td>nginx</td>
    <td>tutum/nginx</td> 
    <td>nome_do_projeto_nginx</td>
  </tr>
</table>

Acessando o projeto:
===========

    a) Após a configuração do projeto, abra o navegador de sua preferência com a url http://localhost:8000/api/. Nesta página é possível visualizar endpoints do projeto.
    
    b) O painel administrativo para gestão do sistema esta na url: http://localhost:8000/admin/. Pode efetuar login com user:admin senha:123123.
    
    c) Logado no painel administrativo poderá acessar a url: http://localhost:8000/admin/twitter/tweet/, neste endereço temos uma lista de tweets cadastrados. É possível remover um tweet. 
    
    d) Na url: http://localhost:8000/admin/twitter/tweet/add/, poderá cadastrar um novo tweet.
    

## Como utilizar a API

### Get Authentication Token

<pre>

curl -X POST -d "client_id=YsUns9jisQeb8DIstVIs58XFp4JRADQXG5WrqsVC&client_secret=6xJv7mSNEIPQgSeBsak6isx1oNiwOanQYkVeJSxBoHYoNCfosuupl3ik0oxwWoi5NZ8z9pUDyo9KTAaHyqlSYC8t5zRgvGqjMuSDUFn9EmYoFZm4SQAPdtm0vg1RIj0X&grant_type=password&username=admin&password=123123" http://localhost:8000/auth/token
 
## Exemplo de saída:

{"token_type":"Bearer","refresh_token":"z2KGN0eZVj8JiFlsTNRN7vfd9JXkBP","access_token":"lhxaWs7vG6X07ObguofZqU0rLkbCUi","scope":"write read","expires_in":36000}%
 
</pre>

### Criando um Tweet

<pre>

curl -X POST -H 'Content-Type: application/json' -H 'Authorization: bearer lhxaWs7vG6X07ObguofZqU0rLkbCUi' -d '{"text":"Teste 1", "user":"12"}' http://localhost:8000/api/twitter/

## Exemplo de saída:

{"id":25,"user":12,"text":"Teste 1"}%

</pre>

### Solicitando Todos Tweets

<pre>

curl -X GET -H 'Content-Type: application/json' -H 'Authorization: bearer lhxaWs7vG6X07ObguofZqU0rLkbCUi' http://localhost:8000/api/twitter/

## Exemplo de saída:

[{"id":22,"user":12,"text":"Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum"},{"id":23,"user":12,"text":"Lorem_2 ipsum Lorem ipsum Lorem ipsum Lorem ipsum"},{"id":24,"user":12,"text":"bake a bread"}]

</pre>

### Atualizando um Tweet

<pre>

curl -X PUT -H 'Content-Type: application/json' -H 'Authorization: bearer lhxaWs7vG6X07ObguofZqU0rLkbCUi' -d '{"text":"Teste 2", "user":"12"}' http://localhost:8000/api/twitter/22/

## Exemplo de saída:

{"id":22,"user":12,"text":"Teste 2"}%
</pre> 



Testes automatizados
====================

Os testes realizados nesse projetos são unitários e de E2E (End to End). 
Para estes últimos, optei por usar um Headless Browser - nesse caso, [PhamtonJS](http://phantomjs.org/download.html). 
Baixo-o e coloque o binário no PATH e pronto.
Há testes também realizados com o Chrome Driver, então você deve instalá-lo do seu PATH também.
